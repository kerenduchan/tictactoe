/*
 * action types
 */
export const CELL_CLICKED = "CELL_CLICKED";
export const GO_BACK = "GO_BACK";
export const GO_FORWARD = "GO_FORWARD";

/*
 * action creators
 */

export function cellClicked(rowId, columnId) {
  return { type: CELL_CLICKED, rowId, columnId };
}

export function goBack() {
  return { type: GO_BACK };
}

export function goForward() {
  return { type: GO_FORWARD };
}
