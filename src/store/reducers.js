import { getNewGameState, makeMove } from "../logic/TictactoeEngine";

import { CELL_CLICKED, GO_BACK, GO_FORWARD } from "./actions";

const initialState = {
  history: [getNewGameState()],
  currentGameStateIdx: 0
};

function tictactoeGame(state = initialState, action) {
  let idx = state.currentGameStateIdx;

  switch (action.type) {
    case CELL_CLICKED:
      const currentGameState = state.history[idx];
      const newGameState = makeMove(
        currentGameState,
        action.rowId,
        action.columnId
      );
      if (newGameState === currentGameState) {
        return state;
      }
      idx++;
      const history = state.history.slice(0, idx).concat([newGameState]);
      console.log("idx", idx, "history", history);
      return {
        history,
        currentGameStateIdx: idx
      };

    case GO_BACK:
      console.log("GO_BACK");
      return idx === 0
        ? state
        : {
            ...state,
            currentGameStateIdx: idx - 1
          };

    case GO_FORWARD:
      console.log("GO_FORWARD");
      return idx === state.history.length - 1
        ? state
        : {
            ...state,
            currentGameStateIdx: idx + 1
          };

    default:
      return state;
  }
}

export default tictactoeGame;
