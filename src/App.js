import React from 'react';
import './App.css';
import TictactoeApp from './components/TictactoeApp'

function App() {
  return (
    <div className="app">
      <TictactoeApp />
    </div>
  );
}

export default App;
