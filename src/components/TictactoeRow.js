import React from "react";
import TictactoeCell from "./TictactoeCell";

function TictactoeRow(props) {
  let cellsJsx = [];
  for (let i = 0; i < 3; i++) {
    const cell = props.rowContents[i];
    cellsJsx.push(
      <TictactoeCell
        key={i}
        rowId={props.rowId}
        columnId={i}
        cellContents={cell}
        isGameOver={props.isGameOver}
        handleCellClicked={props.handleCellClicked}
      />
    );
  }

  return <div className="ttt-row">{cellsJsx}</div>;
}

export default TictactoeRow;
