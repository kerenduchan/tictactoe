import React from "react";
import { connect } from "react-redux";
import TictactoeBoard from "./TictactoeBoard";
import TictactoeStatus from "./TictactoeStatus";
import TictactoeHistoryController from "./TictactoeHistoryController";
import { cellClicked, goBack, goForward } from "../store/actions";

class TictactoeApp extends React.Component {
  render() {
    console.log(this.props);
    const idx = this.props.idx;
    const gameState = this.props.gameState;
    const goBack = idx === 0 ? null : this.props.handleGoBack;
    const goForward =
      idx === this.props.maxIdx ? null : this.props.handleGoForward;

    return (
      <div>
        <TictactoeStatus
          player={gameState.player}
          winner={gameState.winner}
          isGameOver={gameState.isGameOver}
        />

        <TictactoeBoard
          board={gameState.board}
          isGameOver={gameState.isGameOver}
          handleCellClicked={this.props.handleCellClicked}
        />

        <TictactoeHistoryController goBack={goBack} goForward={goForward} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log("state", state);
  const idx = state.currentGameStateIdx;
  return {
    idx,
    maxIdx: state.history.length - 1,
    gameState: state.history[idx]
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleCellClicked: (rowId, columnId) =>
      dispatch(cellClicked(rowId, columnId)),
    handleGoBack: () => dispatch(goBack()),
    handleGoForward: () => dispatch(goForward())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TictactoeApp);
