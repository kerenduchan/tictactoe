import React, { Component } from 'react';
class TictactoeStatus extends Component {

    render() {
        let text = '';
        if (this.props.isGameOver) {
            text = 'Game over. '
                + (this.props.winner === null ? 'Nobody' : this.props.winner)
                + ' won.';
        } else {
            text = this.props.player + "'s turn";
        }
        return (
            <div className="ttt-state">
                <p> {text}</p>
            </div>
        );
    }
}


export default TictactoeStatus;
