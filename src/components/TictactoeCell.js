import React from "react";

// represents one cell in the Tic Tac Toe board (UI component).
// receives through props:
// - state - one of:
//   '' - indicates an empty cell
//   'X' or 'O' - indicates an occupied cell (occupied by either X or O)
//   'X-winning' or 'O-winning' - indicates a cell that's part of the winning row filled with either X or O
// - rowId, columnId - 0, 1, or 2 - the index of the row / column for this cell
// - text - the text inside the cell ('', 'X', or 'O')
// - handleCellClicked - should be invoked when the cell is clicked. Parameters: rowId, columnId of this cell

function TictactoeCell(props) {
  const text = props.cellContents.text;

  let className = "ttt-cell ttt-cell-";
  if (text) {
    // 'X' or 'O' cell
    className += text;
    if (props.cellContents.isWinningCell) {
      className += "-winning";
    }
  } else {
    // empty cell
    className += "empty";
    if (props.isGameOver) {
      className += "-disabled";
    }
  }

  return (
    <div
      className={className}
      onClick={() => props.handleCellClicked(props.rowId, props.columnId)}
    >
      {text}
    </div>
  );
}

export default TictactoeCell;
