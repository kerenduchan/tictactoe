import React from "react";
import TictactoeRow from "./TictactoeRow";

function TictactoeBoard(props) {
  let rowsJsx = [];
  for (let i = 0; i < 3; i++) {
    rowsJsx.push(
      <TictactoeRow
        key={i}
        rowId={i}
        rowContents={props.board[i]}
        isGameOver={props.isGameOver}
        handleCellClicked={props.handleCellClicked}
      />
    );
  }

  return <div className="ttt-table">{rowsJsx}</div>;
}

export default TictactoeBoard;
