import React from "react";

function TictactoeHistoryController(props) {
  console.log("TictactoeHistoryController", props);
  return (
    <div className="ttt-history-controller">
      <button
        className="ttt-history-controller-button"
        disabled={props.goBack === null}
        onClick={props.goBack}
      >
        back
      </button>

      <button
        className="ttt-history-controller-button"
        disabled={props.goForward === null}
        onClick={props.goForward}
      >
        forward
      </button>
    </div>
  );
}

export default TictactoeHistoryController;
