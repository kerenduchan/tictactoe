const cellRangesForWinning = [
  [[0, 0], [0, 1], [0, 2]],
  [[1, 0], [1, 1], [1, 2]],
  [[2, 0], [2, 1], [2, 2]],
  [[0, 0], [1, 0], [2, 0]],
  [[0, 1], [1, 1], [2, 1]],
  [[0, 2], [1, 2], [2, 2]],
  [[0, 0], [1, 1], [2, 2]],
  [[0, 2], [1, 1], [2, 0]]
];

function getNewGameState() {
  const board = new Array(3).fill(
    new Array(3).fill({
      text: "",
      isWinningCell: false
    })
  );

  return {
    board,
    player: "X",
    winner: null,
    isGameOver: false
  };
}

function cloneGameState(gameState) {
  return {
    ...gameState,
    board: gameState.board.map(row => row.map(cell => ({ ...cell })))
  };
}

function getCellText(gameState, rowId, columnId) {
  return gameState.board[rowId][columnId].text;
}

function makeMove(gameState, rowId, columnId) {
  if (gameState.isGameOver || getCellText(gameState, rowId, columnId) !== "") {
    return gameState;
  }

  const newGameState = cloneGameState(gameState);
  newGameState.board[rowId][columnId].text = newGameState.player;
  newGameState.player = newGameState.player === "X" ? "O" : "X";

  // check if somebody won
  const winningCells = findWinningCells(newGameState);
  newGameState.isGameOver =
    winningCells !== null || !hasEmptyCells(newGameState);

  if (winningCells) {
    for (const [row, col] of winningCells) {
      newGameState.board[row][col].isWinningCell = true;
      newGameState.winner = newGameState.board[row][col].text;
    }
  }

  return newGameState;
}

function findWinningCells(gameState) {
  for (const range of cellRangesForWinning) {
    let cells = [];
    for (const [row, col] of range) {
      cells.push(getCellText(gameState, row, col));
    }

    if (cells[0] === "") {
      continue;
    }
    if (cells[0] === cells[1] && cells[1] === cells[2]) {
      // found a winning range of cells
      return range;
    }
  }
  // no winning range of cells was found
  return null;
}

function hasEmptyCells(gameState) {
  return gameState.board.some(row => row.some(cell => cell.text === ""));
}

export { getNewGameState, cloneGameState, makeMove };
